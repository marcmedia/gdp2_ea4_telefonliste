

import java.util.ArrayList;

public class Telefonliste {
	
	private ArrayList<TelefonEintrag> phoneList = new ArrayList<TelefonEintrag>();
	
	
	public int size() {
		return phoneList.size();
	}
	
	public void clear() {
		phoneList.clear();
	}
	
	
	public void addEntry(String name, String number) {
		phoneList.add(new TelefonEintrag(name, number));
	}
	
	
	public boolean modifyEntry(String oldName, String oldNumber, String newName, String newNumber) {
		boolean modifySuccessful = false;
		for (int i = 0; i < size(); i++) {
			if (phoneList.get(i).getName().equals(oldName) && 
					phoneList.get(i).getNumber().equals(oldNumber)) {
				phoneList.get(i).setName(newName);
				phoneList.get(i).setNumber(newNumber);
				modifySuccessful = true;
			}
		}
		return modifySuccessful;
	}
	
	
	public void removeEntry(String name, String number) {
		for (int i = 0; i < size(); i++) {
			if (phoneList.get(i).getName().equals(name) &&
					phoneList.get(i).getNumber().equals(number)) {
				phoneList.remove(i);
			}	
		}
	}
	
	
	public String searchByName(String name) {
		String entry = null;
		for (int i = 0; i < size(); i++) {
			if (phoneList.get(i).getName().equals(name)) {
				String foundName = phoneList.get(i).getName();
				String foundNumber = phoneList.get(i).getNumber();
				
				entry = "Name: " + foundName + " Nummer: " + foundNumber;
			}
		}
		return entry;
	}
	
	
	public String searchNameByNumber(String number) {
		String entry = null;
		for (int i = 0; i < size(); i++) {
			if (phoneList.get(i).getNumber().equals(number)) {
				String foundName = phoneList.get(i).getName();
				String foundNumber = phoneList.get(i).getNumber();
				
				entry = "Name: " + foundName + " Nummer: " + foundNumber;
			}
		}
		return entry;
	}
}
